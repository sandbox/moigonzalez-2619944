<?php

/**
 * @file
 * Defines settings for the Instagram feed from User API.
 */

/**
 * Function to get the instagram information from the user.
 */
function instagram_import_settings_form($form, $form_state) {
  $form['instagram_import_client_id'] = array(
    '#title' => t('Instagram client ID'),
    '#type' => 'textfield',
    '#description' => t('You have to get this ID from your Instagram app'),
    '#default_value' => variable_get('instagram_import_client_id', ""),
  );

  $form['instagram_import_users_list'] = array(
    '#title' => t('Instagram user list'),
    '#type' => 'textarea',
    '#description' => t('Enter the users, separated by a coma'),
    '#default_value' => variable_get('instagram_import_users_list', array()),
  );

  $form['instagram_import_hastag_list'] = array(
    '#title' => t('Hashtags to filter the posts'),
    '#type' => 'textarea',
    '#description' => t('Enter the hastags, separated by a coma'),
    '#default_value' => variable_get('instagram_import_hastag_list', array()),
  );

  return system_settings_form($form);
}
