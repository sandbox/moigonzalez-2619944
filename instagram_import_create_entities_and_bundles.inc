<?php

/**
 * @file
 * Creation of content types and fields.
 */

/**
 * Defines bundle Instagram Import Post and creates fields for bundles.
 */
function instagram_import_create_entities_and_bundles() {
  $types = array();
  $types[] = array(
    'type' => 'instagram_import_post',
    'name' => 'Instagram Post',
    'base' => 'node_content',
    'module' => 'instagram_import',
    'description' => 'These are the Instagram posts collected by the Instagram Import module.',
    'help' => '',
    'has_title' => '1',
    'title_label' => 'Post Title',
    'custom' => '1',
    'modified' => '0',
    'locked' => '0',
    'disabled' => '0',
    'orig_type' => 'instagram_import_post',
    'disabled_changed' => '',
  );

  foreach ($types as $type) {
    if (node_type_load($type['type']) == FALSE) {
      $type = node_type_set_defaults($type);
      node_type_save($type);
    }
  }

  if (field_info_field('field_instagram_id') == FALSE) {
    $field = array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(),
      'field_name' => 'field_instagram_id',
      'type' => 'text',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'bundles' => array(
        'node' => array(
          '0' => 'instagram_import_post',
        ),
      ),
    );
    $field = field_create_field($field);
  }

  if (field_info_instance('node', 'field_instagram_id', 'instagram_import_post') == NULL) {
    $instance = array(
      'label' => 'Instagram Post ID',
      'required' => '0',
      'widget' => array(
        'active' => '1',
        'module' => 'text',
        'type' => 'text_textfield',
        'weight' => '2',
        'settings' => array(
          'size' => '60',
        ),
      ),
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => '',
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'text_default',
          'weight' => '9',
          'settings' => array(),
          'module' => 'text',
        ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'settings' => array(),
          'weight' => '0',
        ),
      ),
      'description' => '',
      'default_value' => '',
      'field_name' => 'field_instagram_id',
      'entity_type' => 'node',
      'bundle' => 'instagram_import_post',
      'deleted' => '0',
    );
    $field = field_create_instance($instance);
  }

  if (field_info_field('field_instagram_image') == FALSE) {
    $field = array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'uri_scheme' => 'public',
        'default_image' => '0',
      ),
      'field_name' => 'field_instagram_image',
      'type' => 'image',
      'module' => 'image',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'bundles' => array(
        'node' => array(
          '0' => 'instagram_import_post',
        ),
      ),
    );
    $field = field_create_field($field);
  }

  if (field_info_instance('node', 'field_instagram_image', 'instagram_import_post') == NULL) {
    $instance = array(
      'label' => 'Post Image',
      'widget' => array(
        'weight' => '6',
        'type' => 'image_image',
        'module' => 'image',
        'active' => '1',
        'settings' => array(
          'progress_indicator' => 'throbber',
          'preview_image_style' => 'thumbnail',
        ),
      ),
      'settings' => array(
        'file_directory' => '',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '',
        'min_resolution' => '',
        'alt_field' => '0',
        'title_field' => '0',
        'default_image' => '0',
        'user_register_form' => '',
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'image',
          'settings' => array(
            'image_style' => '',
            'image_link' => '',
          ),
          'module' => 'image',
          'weight' => '15',
        ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'settings' => array(),
          'weight' => '0',
        ),
      ),
      'required' => '0',
      'description' => '',
      'field_name' => 'field_instagram_image',
      'entity_type' => 'node',
      'bundle' => 'instagram_import_post',
      'deleted' => '0',
    );
    field_create_instance($instance);
  }

  if (field_info_field('field_instagram_caption') == FALSE) {
    $field = array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(),
      'field_name' => 'field_instagram_caption',
      'type' => 'text_long',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'bundles' => array(
        'node' => array(
          '0' => 'instagram_import_post',
        ),
      ),
    );
    $field = field_create_field($field);
  }
  if (field_info_instance('node', 'field_instagram_caption', 'instagram_import_post') == NULL) {
    $instance = array(
      'label' => 'Post Caption',
      'required' => '0',
      'widget' => array(
        'active' => '1',
        'module' => 'text',
        'type' => 'text_textarea',
        'weight' => '7',
        'settings' => array(
          'rows' => '3',
        ),
      ),
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => '',
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'emoji_formatter',
          'weight' => '6',
          'settings' => array(),
          'module' => 'instagram_import',
        ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'settings' => array(),
          'weight' => '0',
        ),
      ),
      'description' => '',
      'default_value' => '',
      'field_name' => 'field_instagram_caption',
      'entity_type' => 'node',
      'bundle' => 'instagram_import_post',
      'deleted' => '0',
    );
    field_create_instance($instance);
  }

  if (field_info_field('field_instagram_creation_date') == FALSE) {
    $field = array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array(
        'todate' => '',
        'granularity' => array(
          'year' => 'year',
          'month' => 'month',
          'day' => 'day',
          'hour' => 'hour',
          'minute' => 'minute',
        ),
        'tz_handling' => 'site',
        'timezone_db' => 'UTC/GMT',
      ),
      'field_name' => 'field_instagram_creation_date',
      'type' => 'datetime',
      'module' => 'date',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'bundles' => array(
        'node' => array(
          '0' => 'instagram_import_post',
        ),
      ),
    );
    $field = field_create_field($field);
  }
  if (field_info_instance('node', 'field_instagram_creation_date', 'instagram_import_post') == NULL) {
    $instance = array(
      'label' => 'Creation Date',
      'required' => '0',
      'widget' => array(
        'active' => '1',
        'module' => 'date',
        'type' => 'date_text',
        'weight' => '13',
        'settings' => array(
          'increment' => '15',
          'input_format' => 'Y-m-d H:i:s',
          'input_format_custom' => '',
          'label_position' => 'above',
          'text_parts' => array(),
          'year_range' => '-3:+3',
        ),
      ),
      'settings' => array(
        'default_value' => 'now',
        'default_value_code' => '',
        'default_value2' => 'same',
        'default_value_code2' => '',
        'user_register_form' => '',
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'date_default',
          'weight' => '7',
          'settings' => array(
            'format_type' => 'long',
            'multiple_number' => '',
            'multiple_from' => '',
            'multiple_to' => '',
            'fromto' => 'both',
          ),
          'module' => 'date',
        ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'settings' => array(),
          'weight' => '0',
        ),
      ),
      'description' => '',
      'field_name' => 'field_instagram_creation_date',
      'entity_type' => 'node',
      'bundle' => 'instagram_import_post',
      'deleted' => '0',
    );
    field_create_instance($instance);
  }

  if (field_info_field('field_link_to_instagram_post') == FALSE) {
    $field = array(
      'translatable' => '0',
      'entity_types' => array(),
      'settings' => array('max_length' => '255',
      ),
      'field_name' => 'field_link_to_instagram_post',
      'type' => 'link_field',
      'module' => 'text',
      'active' => '1',
      'locked' => '0',
      'cardinality' => '1',
      'deleted' => '0',
      'bundles' => array(
        'node' => array(
          '0' => 'instagram_import_post',
        ),
      ),
    );
    $field = field_create_field($field);
  }
  if (field_info_instance('node', 'field_link_to_instagram_post', 'instagram_import_post') == NULL) {
    $instance = array(
      'label' => 'Link To Post',
      'required' => '0',
      'widget' => array(
        'active' => '1',
        'module' => 'text',
        'type' => 'link',
        'weight' => '17',
        'settings' => array(
          'size' => '60',
        ),
      ),
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => '',
      ),
      'display' => array(
        'default' => array(
          'label' => 'above',
          'type' => 'text_default',
          'weight' => '3',
          'settings' => array(),
          'module' => 'text',
        ),
        'teaser' => array(
          'type' => 'hidden',
          'label' => 'above',
          'settings' => array(),
          'weight' => '0',
        ),
      ),
      'description' => '',
      'default_value' => '',
      'field_name' => 'field_link_to_instagram_post',
      'entity_type' => 'node',
      'bundle' => 'instagram_import_post',
      'deleted' => '0',
    );
    field_create_instance($instance);
  }

  cache_clear_all();
}
